import { Get, Route } from "tsoa";

interface DnaMutationResponse {
    message: string;
}

@Route("ping")
export default class DnaMutationController {
    @Get("/")
    public async getMessage(): Promise<DnaMutationResponse> {
        return {
            message: "pong",
        };
    }
}
