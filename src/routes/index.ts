import express from "express";
import DnaMutationController from "../controller/DnaMutationController";

const router = express.Router();

router.get("/ping", async (_req, res) => {
  const controller = new DnaMutationController();
  const response = await controller.getMessage();
  return res.send(response);
});

export default router;